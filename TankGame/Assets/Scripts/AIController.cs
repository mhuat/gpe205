﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIController : MonoBehaviour
{
    [Header("AI Controller")]

    [Header("Tank Data")]

    //Booleans
    public bool canFire;
	public bool canSee;

	//[Header("Tank Data")]*******
    //Number Values

	//GameObjects

    //Instances
    public TankData tankData;

    //Static Variables

    //Transforms
    private Transform m_target;
    private NavMeshAgent m_agent;
	
	void Start ()
	{
	    m_agent = GetComponent<NavMeshAgent>(); //Initialize m_agent.
        tankData = GetComponent<TankData>(); //Initialize TankData Instance.
    }
	


	void Update ()
	{
		if (canFire == false) //If the tank does not the have a shell to shoot.
		{
			LoadingShell ();
		}
		if (canFire&&canSee) //Is the shell loaded and can the tank see the player
		{
			FireShell ();
		}
		if (canFire)
		{
			CanSeePlayer ();
		}
        //If player is not null; move towards target, else, move towards starting position.
	    if (GameManager.instance.player != null)
	    {
			m_target = GameManager.instance.pC.transform; //AI's target is the Player instance referenced in the Game Manager instance.
	        MoveToTarget();
            LookAtPlayer();
        }else{ MoveToStart(); }
	}

    void MoveToTarget() //Chase the Player instance.
    {
        m_agent.destination = m_target.position; //Takes he agent component's destination of the AI and sets it to target's updated position.
    }

    void MoveToStart() //Return to spawn location.
    {
        m_agent.destination = GameManager.instance.enemySpawnLocation.transform.position;
    }

    /// <summary>
    /// Declares and Initializes "lookVector" to the difference referenced.
    /// Declares and Initializes "rotateTowards" to equal the set forward and upwards direction.
    /// Quaternion.Slerp() clamps a range from point a to point b.
    /// </summary>
    void LookAtPlayer()
    {
        Vector3 lookVector = GameManager.instance.player.transform.position - transform.position;
        Quaternion rotateTowards = Quaternion.LookRotation(lookVector);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotateTowards, 1);
    }

    /*
    void OnDestroy() //Remove this object from the L_Enemy list when destroyed.
    {
        GameManager.instance.L_Enemy.Remove(this.gameObject);
    }*/

	private void FireShell()
	{
		if (canFire)
		{
			GameObject shellInstance = Instantiate(tankData.shellPrefab, tankData.tankBarrel.transform.position, tankData.tankBarrel.transform.rotation);//Instantiate shellprefab at tank barrel transform position/rotation. 
			shellInstance.GetComponent<Rigidbody>().AddForce(shellInstance.transform.forward * tankData.m_shellForce * Time.deltaTime, ForceMode.Impulse); //Addforce to shellinstance's rigidbody. 
			shellInstance.tag = "Enemy Shell";//Tag bulletinstanace.
			canFire = false;
		}
	}

	private void LoadingShell()
	{
		tankData.m_fireRate -= Time.deltaTime;
		if (tankData.m_fireRate < 0f)
		{
			canFire = true;
            tankData.m_fireRate = tankData.m_resetValue;
		}
	}

	private void CanSeePlayer()
	{
		Vector3 targetDir = GameManager.instance.player.transform.position-this.transform.position; //The difference from the player instance's position to this gameObject's position results in target's direction. 
		Debug.Log(targetDir);
		float angleToPlayer = Vector3.Angle(targetDir, transform.forward); //Reference to the angle shot in the player intance's direction.
		Debug.Log(angleToPlayer);
		if (angleToPlayer >= 0 && angleToPlayer <= 180) /* 180° FOV*/ { Debug.Log("Player in sight!"); canSee = true; } //If the angle is in the FOV, Debug & canSee equals true.
	}

}