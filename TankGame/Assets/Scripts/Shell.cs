﻿using UnityEngine;

public class Shell : MonoBehaviour
{
    public float m_lifeSpan; // Time the shell is alive.
	public float m_shellDamage; // Damage that's added to any source's base damage.

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        m_lifeSpan -= Time.deltaTime;
        if (m_lifeSpan < 0f)
        {
            Destroy(gameObject);
        }
    }

    public void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.GetComponent<Health>()!=null && this.tag == "Player Shell" && other.gameObject!=GameManager.instance.player)
        {
			other.gameObject.GetComponent<Health>().m_health -= GameManager.instance.player.GetComponent<TankData>().m_tankDamage+m_shellDamage;
            GameManager.instance.player.GetComponent<TankData>().m_score += GameManager.instance.aC.GetComponent<TankData>().m_pointValue;
            Destroy(gameObject);
        }
		if (other.gameObject.GetComponent<Health>()!=null && this.tag == "Enemy Shell" && other.gameObject.GetComponent<AIController>()==null) //&& other.gameObject!=gameObject.CompareTag("Enemy"))
		{
			other.gameObject.GetComponent<Health>().m_health -= GameManager.instance.aC.GetComponent<TankData>().m_tankDamage+m_shellDamage;
			Destroy(gameObject);
		}
    }

}

//Notes:
//GameManager.instance.enemyHealth -= GameManager.instance.playerDamage;
//enemyRef.GetComponent<Health>().m_health -= playerRef.GetComponent<PlayerController>().m_shellDamage;
//GameManager.instance.aC.health -= GameManager.instance.pC.m_Damage;
/*
    if (other.gameObject.CompareTag("Player") && this.tag == "Enemy Shell")
    {
        GameManager.instance.enemy.GetComponent<Health>().m_health = GameManager.instance.enemy.GetComponent<Health>().m_health -
        GameManager.instance.enemy.GetComponent<AIController>().m_shellDamage;
        Destroy(gameObject);
    }
    if (other.gameObject.CompareTag("Enemy") && this.tag == "Player Shell")
    {
        GameManager.instance.player.GetComponent<Health>().m_health -=
            GameManager.instance.player.GetComponent<PlayerController>().m_shellDamage;
        Destroy(gameObject);
    }
    if (other.gameObject.CompareTag("Enemy") && this.tag == "Player Shell")
    {
        //other.gameObject.GetComponent<Health>().m_health-=GameManager.instance.pC.m_damage;
        Destroy(gameObject);
    }
    if (other.gameObject.CompareTag("Player") && this.tag == "Enemy Shell")
    {
        //GameManager.instance.player.GetComponent<Health>().m_health -=
        //GameManager.instance.player.GetComponent<PlayerController>().m_shellDamage;
        Destroy(gameObject);
    }
*/
