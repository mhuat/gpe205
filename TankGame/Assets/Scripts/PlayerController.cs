﻿using JetBrains.Annotations;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Player Controller")]

    [Header ("Tank Data")]

    //Booleans
    private bool canFire;
    public bool falling;

    //Number Values
    public float m_forwardSpeed; //Move Forward Speed in Inspector.
    public float m_backwardSpeed; //Move Backward Speed in Inspector.
    public float m_rotationSpeed; //Rotation Speed in Inspector.

    //GameObjects

    //Instances
    public TankData tankData;

    //Static Variables

    //RigidBody
    private Rigidbody m_rb; //Declare RigidBody

    private void Start()
    {
        m_rb = GetComponent<Rigidbody>(); //Initialize RigidBody.
        tankData = GetComponent<TankData>(); //Initialize TankData Instance.
    }

    private void Update()
    {
        if (canFire == false)
        {
            LoadingShell();
        }
        //RayCastToGround();
        Debug.Log("Player Score: " + tankData.m_score);
    }

    private void FixedUpdate()
    {
        if (Input.anyKey&&falling==false) //Check if there's any Player Input before executing methods.
        {
            Control();
        }

        if (falling)
        {
            m_rb.velocity -= Vector3.down * Time.deltaTime; //Decrease velocity by desired variable.
            m_rb.mass += Time.deltaTime; //Increase mass by desired variable.
        }
    }

    private void Control()
    {
        if (Input.GetKey(KeyCode.S))
        {
            MoveBackward();
        }

        if (Input.GetKey(KeyCode.W))
        {
            MoveForward();
        }

        if (Input.GetKey(KeyCode.A))
        {
            RotateLeft();
        }

        if (Input.GetKey(KeyCode.D))
        {
            RotateRight();
        }

        if (Input.GetKey(KeyCode.Space))
        {
            FireShell();
        }
    }

    private void MoveBackward()
    {
        float speed = m_backwardSpeed * Time.deltaTime;
        m_rb.velocity = -transform.forward * speed;
    }

    private void MoveForward()
    {
        float speed = m_forwardSpeed * Time.deltaTime;
        m_rb.velocity = transform.forward * speed;
    }

    private void RotateLeft()
    {
        float speed = m_rotationSpeed * Time.deltaTime;
        transform.Rotate(new Vector3(0, -1, 0) * speed, Space.World);
    }
		
    private void RotateRight()
    {
        float speed = m_rotationSpeed * Time.deltaTime;
        transform.Rotate(new Vector3(0, 1, 0) * speed, Space.World);
    }

	//Instantiate projectile.
    private void FireShell()
    {
		if (canFire)//Chceck to see if boolean is true.
        {
            GameObject shellInstance = Instantiate(tankData.shellPrefab, tankData.tankBarrel.transform.position, tankData.tankBarrel.transform.rotation); //Instantiate Shellprefab at the player's barrel transform position/rotation. 
			shellInstance.GetComponent<Rigidbody> ().AddForce (shellInstance.transform.forward * tankData.m_shellForce * Time.deltaTime, ForceMode.Impulse); //BulletInstance RigidBody reference.
            shellInstance.tag = "Player Shell"; //Tag BulletInstance
			canFire = false; //Unable to Fire, Commence LoadingShell()
        }
    }

	//RELOAD
    private void LoadingShell()
    {
        tankData.m_fireRate -= Time.deltaTime;
        if (tankData.m_fireRate < 0f)
        {
            canFire = true;
            tankData.m_fireRate = tankData.m_resetValue;
        }
    }

    /// <summary>
    /// On Collision with other objects, trigger corresponding event.
    /// </summary>
    /// <param name="other"></param>
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Death Zone"))
        {
            GameManager.instance.m_isDead = true;
            Destroy(GameManager.instance.player);
        }
    }

	/// <summary>
	/// Casts a ray from thje player to the ground at an infinite distance.
	/// If a death zone is hit with the raycast, Do: ("falling = true")
	/// </summary>
    void RayCastToGround()
    {
        RaycastHit hit = new RaycastHit();
        Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
        Debug.Log(hit);
        RaycastHit ground = new RaycastHit();
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out ground, Mathf.Infinity))
        {
            if (ground.collider.tag == "Death Zone")
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.down) * ground.distance,
                    Color.blue);
                Debug.Log("Ground Hit");
                falling = true;
            }
        }
    }

}
